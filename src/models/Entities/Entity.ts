import {Point} from "../Point";

export abstract class Entity {

    public coord: Point;
    public id: number;
    public vx: number;
    public vy: number;
    public state: boolean;

    constructor({entityId, x, y, vx, vy, state}) {
        this.coord = new Point({x, y});
        this.id = entityId;
        this.vx = vx;
        this.vy = vy;
        this.state = state;
    }

}
