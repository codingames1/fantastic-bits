import {Strategy} from "./Strategies/Strategy";
import {Move} from "./Strategies/Move";
import {Throw} from "./Strategies/Throw";
import {Wizard} from "./Entities/Wizard";
import {Snaffle} from "./Entities/Snaffle";
import {Point} from "./Point";
import {Petrificus} from "./Strategies/Spell/Petrificus";
import {Bludger} from "./Entities/Bludger";

export class Game {
    protected wizards: Wizard[] = [];
    protected wizardEnemies: Wizard[] = [];
    protected snaffles: Snaffle[] = [];
    protected goalEnemy: Point;
    protected myMagic: number;
    protected bludgers: Bludger[];

    public nextAction(wizard: Wizard): string {
        const params = {
            wizard,
            snaffles: this.snaffles,
            goalEnemy: this.goalEnemy,
            wizardEnemies: this.wizardEnemies
        };
        let strategy: Strategy;
        if(this.enoughPointToUseSpell()) {
            strategy = new Petrificus(params);
        }else if(Game.haveToThrow(wizard)) {
            strategy = new Throw(params);
        }else{
            strategy = new Move(params);
        }

        return strategy.nextAction();
    }

    private static haveToThrow(wizard: Wizard): boolean {
        return wizard.state;
    }

    public setSnaffles(snaffles: Snaffle[]): void {
        this.snaffles = snaffles;
    }

    public setWizards(wizards: Wizard[]): void {
        this.wizards = wizards;
    }

    public setWizardEnemies(wizards: Wizard[]): void {
        this.wizardEnemies = wizards;
    }

    public setMagic(myMagic: number): void {
        this.myMagic = myMagic;
    }

    public setBludgers(bludgers: Bludger[]): void {
        this.bludgers = bludgers;
    }

    public setGoalEnemy(team: number): void {
        if(team === 1) {
            this.goalEnemy = new Point({x: 0, y:3750});
        }else {
            this.goalEnemy = new Point({x: 16000, y:3750});
        }
    }

    public findWizardById = (id: number): Wizard => this.wizards.filter((entity) => entity.id === id)[0];

    private enoughPointToUseSpell(): boolean {
        return this.myMagic >= 10;
    }
}
