import {Strategy} from "./Strategy";
import {Point} from "../Point";

export class Throw extends Strategy {

    public nextAction(): string {
        const point: Point = this.goalEnemy;

        return `THROW ${point.x} ${point.y} 500`;
    }
}
