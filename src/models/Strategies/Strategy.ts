import {Snaffle} from "../Entities/Snaffle";
import {Wizard} from "../Entities/Wizard";
import {Point} from "../Point";
import {distanceBetweenTwoPoints} from "../../services/distance";

export abstract class Strategy {

    protected snaffles: Snaffle[];
    protected wizard: Wizard;
    protected goalEnemy: Point;
    protected wizardEnemies: Wizard[];

    constructor({wizard, snaffles, goalEnemy, wizardEnemies}: {
        wizard: Wizard,
        snaffles : Snaffle[],
        goalEnemy: Point,
        wizardEnemies: Wizard[]
    }) {
        this.snaffles = snaffles;
        this.wizard = wizard;
        this.goalEnemy = goalEnemy;
        this.wizardEnemies = wizardEnemies;
    }

    abstract nextAction(): string

    protected distanceFromAnotherEntity = (snaffle: Snaffle): number => {
        return distanceBetweenTwoPoints(this.wizard.coord, snaffle.coord);
    };

}
