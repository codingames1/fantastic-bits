import {Strategy} from "./Strategy";
import {Point} from "../Point";
import {Snaffle} from "../Entities/Snaffle";

export class Move extends Strategy {

    public nextAction(): string {
        const snaffleToGo: Snaffle = this.goToSnaffle();

        this.snaffles = this.snaffles.map(snaffle => {
            if(snaffleToGo.id === snaffle.id) {
                snaffle.wizards.push(this.wizard);
            }

            return snaffle;
        });

        const point: Point = snaffleToGo.coord;

        return `MOVE ${point.x} ${point.y} 150`;
    }

    private goToSnaffle(): Snaffle {

        const snaffles = this.snaffles
            .filter(snaffle => {
                return snaffle.wizards.length === 0;
            })
            .sort((a, b) => {
                return this.distanceFromAnotherEntity(a) - this.distanceFromAnotherEntity(b);
            });

        if(snaffles.length === 0) {
            return this.snaffles[0];
        }
        return snaffles[0];
    }

}
