import {Spell} from "./Spell";

export class Accio extends Spell {

    public cost: number = 15;
    public duration: number = 6;

    public nextAction(): string {
        const snaffle = this.snaffles[0];

        return `ACCIO ${snaffle.id}`;
    }

}
