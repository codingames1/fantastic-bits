import {Spell} from "./Spell";

export class Petrificus extends Spell {

    public cost: number = 10;
    public duration: number = 1;

    public nextAction(): string {
        const wizardEnemy = this.wizardEnemies[0];

        return `PETRIFICUS ${wizardEnemy.id}`;
    }

}
