import {Spell} from "./Spell";

export class Obliviate extends Spell {

    public cost: number = 5;
    public duration: number = 4;

    public nextAction(): string {
        const snaffle = this.snaffles[0];

        return `OBLIVIATE ${snaffle.id}`;
    }

}
