import {Spell} from "./Spell";

export class Flipendo extends Spell {

    public cost: number = 20;
    public duration: number = 3;

    public nextAction(): string {
        const snaffle = this.snaffles[0];

        return `FLIPENDO ${snaffle.id}`;
    }

}
