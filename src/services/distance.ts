import { Point } from "../models/Point";

export const distanceBetweenTwoPoints = (point1: Point, point2: Point): number => {
    const a = point1.x - point2.x;
    const b = point1.y - point2.y;
    const c = Math.pow(a, 2) + Math.pow(b, 2);
    return Math.pow(c, 0.5);
};
