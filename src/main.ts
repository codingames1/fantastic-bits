import {Game} from "./models/Game";
import {Wizard} from "./models/Entities/Wizard";
import {Snaffle} from "./models/Entities/Snaffle";
import {Bludger} from "./models/Entities/Bludger";

const game = new Game();

/**
 * Grab Snaffles and try to throw them through the opponent's goal!
 * Move towards a Snaffle and use your team id to determine where you need to throw it.
 *
 */

// @ts-ignore
const myTeamId = parseInt(readline(), 10); // if 0 you need to score on the right of the map, if 1 you need to score on the left

game.setGoalEnemy(myTeamId);

// game loop
while (true) {
    // @ts-ignore
    const inputs = readline().split(' ');
    const myScore = parseInt(inputs[0], 10);
    const myMagic = parseInt(inputs[1], 10);
    // @ts-ignore
    const inputs2 = readline().split(' ');
    const opponentScore = parseInt(inputs2[0], 10);
    const opponentMagic = parseInt(inputs2[1], 10);

    game.setMagic(myMagic);

    // @ts-ignore
    const entities = parseInt(readline(), 10); // number of entities still in game

    const snaffles: Snaffle[] = [];
    const wizards: Wizard[] = [];
    const wizardEnemies: Wizard[] = [];
    const bludgers: Bludger[] = [];

    for (let i = 0; i < entities; i++) {
        // @ts-ignore
        const inputs = readline().split(' ');
        const entityId = parseInt(inputs[0], 10); // entity identifier
        const entityType = inputs[1]; // "WIZARD", "OPPONENT_WIZARD" or "SNAFFLE" (or "BLUDGER" after first league)
        const x = parseInt(inputs[2], 10); // position
        const y = parseInt(inputs[3], 10); // position
        const vx = parseInt(inputs[4], 10); // velocity
        const vy = parseInt(inputs[5], 10); // velocity
        const state = parseInt(inputs[6], 10); // 1 if the wizard is holding a Snaffle, 0 otherwise

        const data = {
            entityId,
            entityType,
            x,
            y,
            vx,
            vy,
            state: state === 1
        };

        if(data.entityType === "SNAFFLE") {
            snaffles.push(new Snaffle(data));
        }else if(data.entityType === "WIZARD") {
            wizards.push(new Wizard(data));
        }else if(data.entityType === "OPPONENT_WIZARD") {
            wizardEnemies.push(new Wizard(data));
        }else {
            bludgers.push(new Bludger(data));
        }

        game.setSnaffles(snaffles);
        game.setWizards(wizards);
        game.setWizardEnemies(wizardEnemies);
        game.setBludgers(bludgers);

    }
    for (let i = 0; i < 2; i++) {

        let id = i;
        if(myTeamId === 1) {
            id +=2;
        }

        const wizard: Wizard = game.findWizardById(id);

        /*
         * Edit this line to indicate the action for each wizard (0 ≤ thrust ≤ 150, 0 ≤ power ≤ 500)
         * i.e.: "MOVE x y thrust" or "THROW x y power"
         */
        console.log(game.nextAction(wizard));
    }
}
